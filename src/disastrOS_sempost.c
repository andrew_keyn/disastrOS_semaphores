#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semPost() {
	
	int fd = running->syscall_args[0];

	SemDescriptor* sem_des = SemDescriptor_byFd(&running->sem_descriptors, fd);

	if (!sem_des) {
		running->syscall_retvalue = DSOS_ESEMAPHOREPOST;
		return;
	}

	Semaphore* sem = sem_des->semaphore;

	sem->count++;

	running->syscall_retvalue = 0;

	if (sem->count > 0)
		return;

	SemDescriptorPtr* awoken_ptr = (SemDescriptorPtr*) List_detach(&sem->waiting_descriptors, sem->waiting_descriptors.first);
	assert(awoken_ptr);

	List_insert(&sem->descriptors_ptrs, sem->descriptors_ptrs.last, (ListItem*) awoken_ptr);

	PCB* awoken_pcb = (PCB*) List_detach(&waiting_list, (ListItem*) awoken_ptr->descriptor->pcb);
	assert(awoken_pcb);

	List_insert(&ready_list, ready_list.last, (ListItem*) awoken_pcb);
	awoken_pcb->status = Ready;

	running->status = Ready;
	List_insert(&ready_list, ready_list.last, (ListItem*) running);
	assert(ready_list.first && "Empty ready_list\n");
	running = (PCB*) List_detach(&ready_list, ready_list.first);
}
