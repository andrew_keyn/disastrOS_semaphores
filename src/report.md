# disastrOS_semaphores
Implementation of semaphores for the didactic operative system [disastrOS](https://gitlab.com/grisetti/sistemi_operativi_2016_17).

## Project overview

### What
The aim of the project is to provide an implementation of semaphores to control access to a common resource by multiple processes in disastrOS, the didactic operative system created by [Prof. Giorgio Grisetti](https://sites.google.com/dis.uniroma1.it/grisetti) in the scope of the course [Sistemi Operativi](https://sites.google.com/dis.uniroma1.it/sistemi-operativi/), Fall/Winter 2017, La Sapienza [in italian].

The initial commit is part of the the teaching material of the course.

The execution of the project is going to be the development of the syscalls **internal_semOpen()**, **internal_semClose()**, **internal_semPost()** and **internal_semWait()** and their integration within the system.

### How

Since disastrOS semaphores can be shared amongst different processes through a same id, the semantic implemented is going to be inspired by [Posix Semaphores](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/semaphore.h.html).
#### int disastrOS_semOpen(int semaphore_id, int mode, ...)
If DSOS_CREATE is specified in *mode*, then *count* argument must be supplied. If DSOS_CREATE is specified and a semaphore with the given name already exists, then *count* is ignored. If both DSOS_CREATE and DSOS_EXCL are specified in *mode*, then an error is returned if a semaphore with the given name already exists. Multiple calls within a same process return the same *fd*.
#### int disastrOS_semClose(int semaphore_id)
Removes the link between the calling process and the semaphore identified by *semaphore_id* deallocating the SemDescriptor and SemDescriptor_Ptr associated. If such link is not found an error is returned. If the calling process is the last one using the semaphore, the semaphore is deallocated from the system.
#### int disastrOS_semWait(int fd)
Locks the semaphore referenced by *fd*. If the semaphore value is currently zero, then the calling process shall not return from the call until it locks the semaphore. Upon successful return, the state of the semaphore shall be locked and shall remain locked until the semPost() function is executed and returns successfully.
#### int disastrOS_semPost(int fd)
Unocks the semaphore referenced by *fd*. Upon successful return, one of the processes waiting after a semWait() operation is set Ready for execution and the calling process hands over the cpu to the process on top of ready queue (enhancing disastrOS interleaving).

### How to run

```sh
git clone https://gitlab.com/andrew_keyn/disastrOS_semaphores.git
cd disastrOS_semaphores/src/
make
./disastrOS_semtest
```
