#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semWait() {
	
	int fd = running->syscall_args[0];

	SemDescriptor* sem_des = SemDescriptor_byFd(&running->sem_descriptors, fd);

	if (!sem_des) {
		running->syscall_retvalue = DSOS_ESEMAPHOREWAIT;
		return;
	}

	Semaphore* sem = sem_des->semaphore;

	sem->count--;

	running->syscall_retvalue = 0;

	if (sem->count >= 0)
		return;
	
	ListItem* item = List_detach(&sem->descriptors_ptrs, (ListItem*) sem_des->ptr);
	assert(item);

	List_insert(&sem->waiting_descriptors, sem->waiting_descriptors.last, (ListItem*) sem_des->ptr);

	running->syscall_retvalue = 0;
	running->status = Waiting;
	List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
	assert(ready_list.first && "Empty ready_list\n");
	running = (PCB*) List_detach(&ready_list, ready_list.first);
}
