#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "disastrOS.h"
#include "pool_allocator.h"
#include "linked_list.h"

#define PROD_NUM	4
#define CONS_NUM	5
#define FILL_COUNT	3
#define EMPT_COUNT	4
#define CONS_MUTEX	5
#define PROD_MUTEX	6
#define BUFFER_SIZE	3

static int _buffer[BUFFER_SIZE];
int read_index;
int write_index;

// we need this to handle the sleep state
void sleeperFunction(void* args) {
	while(1) {
		getc(stdin);
		disastrOS_printStatus();
	}
}

void printBuffer() {
	printf("[");
	for (int i = 0; i < BUFFER_SIZE; i++) {
		if (_buffer[i])
			printf(" @ ");
		else
			printf(" _ ");
	}
	printf("]\n");
}

void prodFunction(void* args) {
	int prod_idx = *((int*)args);
	printf("[Producer #%d] Created.\n", prod_idx);

	int ret;

	int fill_count = disastrOS_semOpen(FILL_COUNT, 0);
	//printf("[Producer #%d] disastrOS_semOpen(FILL_COUNT, 0): %d\n", prod_idx, fill_count);
	int empty_count = disastrOS_semOpen(EMPT_COUNT, 0);
	//printf("[Producer #%d] disastrOS_semOpen(EMPT_COUNT, 0): %d\n", prod_idx, empty_count);
	int prod_mutex = disastrOS_semOpen(PROD_MUTEX, 0);
	//printf("[Producer #%d] disastrOS_semOpen(PROD_MUTEX, 0): %d\n", prod_idx, prod_mutex);

	for (int i = 0; i < CONS_NUM; i++){
		printf("[Producer #%d] semWait(empty_count)\n", prod_idx);
		ret = disastrOS_semWait(empty_count);
        //printf("[Producer #%d Iteration %d] disastrOS_semWait(empty_count): %d\n", prod_idx, i, ret);
		printf("[Producer #%d] semWait(prod_mutex)\n", prod_idx);
        ret = disastrOS_semWait(prod_mutex);
        //printf("[Producer #%d Iteration %d] disastrOS_semWait(prod_mutex): %d\n", prod_idx, i, ret);

        _buffer[write_index] = disastrOS_getpid();
        write_index = (write_index + 1) % BUFFER_SIZE;
		printf("[Producer #%d] ", prod_idx);
		printBuffer();
        
		printf("[Producer #%d] semPost(prod_mutex)\n", prod_idx);
		ret = disastrOS_semPost(prod_mutex);
        //printf("[Producer #%d Iteration %d] disastrOS_semPost(prod_mutex): %d\n", prod_idx, i, ret);
		printf("[Producer #%d] semPost(fill_count)\n", prod_idx);
		ret = disastrOS_semPost(fill_count);
        //printf("[Producer #%d Iteration %d] disastrOS_semPost(fill_count): %d\n", prod_idx, i, ret);
	}

	ret = disastrOS_semClose(FILL_COUNT);
	//printf("[Producer #%d] disastrOS_semClose(FILL_COUNT): %d\n", prod_idx, ret);
	ret = disastrOS_semClose(EMPT_COUNT);
	//printf("[Producer #%d] disastrOS_semClose(EMPT_COUNT): %d\n", prod_idx, ret);
	ret = disastrOS_semClose(PROD_MUTEX);
	//printf("[Producer #%d] disastrOS_semClose(PROD_MUTEX): %d\n", prod_idx, ret);

	disastrOS_exit(0);
}

void consFunction(void* args) {
	int cons_idx = *((int*)args);
	printf("[Consumer #%d] Created.\n", cons_idx);

	int ret;

	int fill_count = disastrOS_semOpen(FILL_COUNT, 0);
	//printf("[Consumer #%d] disastrOS_semOpen(FILL_COUNT, 0): %d\n", cons_idx, fill_count);
	int empty_count = disastrOS_semOpen(EMPT_COUNT, 0);
	//printf("[Consumer #%d] disastrOS_semOpen(EMPT_COUNT, 0): %d\n", cons_idx, empty_count);
	int cons_mutex = disastrOS_semOpen(CONS_MUTEX, 0);
	//printf("[Consumer #%d] disastrOS_semOpen(CONS_MUTEX, 0): %d\n", cons_idx, cons_mutex);

	for (int i = 0; i < PROD_NUM; i++){
		printf("[Consumer #%d] semWait(fill_count)\n", cons_idx);
		ret = disastrOS_semWait(fill_count);
        //printf("[Consumer #%d Iteration %d] disastrOS_semWait(fill_count): %d\n", cons_idx, i, ret);
        printf("[Consumer #%d] semWait(cons_mutex)\n", cons_idx);
		ret = disastrOS_semWait(cons_mutex);
        //printf("[Consumer #%d Iteration %d] disastrOS_semWait(cons_mutex): %d\n", cons_idx, i, ret);

        _buffer[read_index] = 0;
        read_index = (read_index + 1) % BUFFER_SIZE;
		printf("[Consumer #%d] ", cons_idx);
		printBuffer();
        
		printf("[Consumer #%d] semPost(cons_mutex)\n", cons_idx);
		ret = disastrOS_semPost(cons_mutex);
        //printf("[Consumer #%d Iteration %d] disastrOS_semPost(cons_mutex): %d\n", cons_idx, i, ret);
		printf("[Consumer #%d] semPost(empty_count)\n", cons_idx);
		ret = disastrOS_semPost(empty_count);
        //printf("[Consumer #%d Iteration %d] disastrOS_semPost(empty_count): %d\n", cons_idx, i, ret);
	}

	ret = disastrOS_semClose(FILL_COUNT);
	//printf("[Consumer #%d] disastrOS_semClose(FILL_COUNT): %d\n", cons_idx, ret);
	ret = disastrOS_semClose(EMPT_COUNT);
	//printf("[Consumer #%d] disastrOS_semClose(EMPT_COUNT): %d\n", cons_idx, ret);
	ret = disastrOS_semClose(CONS_MUTEX);
	//printf("[Consumer #%d] disastrOS_semClose(CONS_MUTEX): %d\n", cons_idx, ret);

	disastrOS_exit(0);
}

void initFunction(void* args) {
	
	printf("Hello, I am init and I just started. For the\nmoment we'll just test semaphores syscalls.\n\n");
	int id[2] = {0, 1};

	printf("\n\nCall: disastrOS_semOpen(0, -1, 0)\nExpected: DSOS_EINVALIDARGUMENT\nReturns: ");
	int openA = disastrOS_semOpen(id[0], -1, 0);
	if (openA == DSOS_EINVALIDARGUMENT)
		printf("DSOS_EINVALIDARGUMENT\n\n");
	else
		printf("%d", openA);

	printf("\n\nCall: disastrOS_semOpen(0, 0)\nExpected: DSOS_ESEMAPHOREOPEN\nReturns: ");
	int openB = disastrOS_semOpen(id[0], 0);
	if (openB == DSOS_ESEMAPHOREOPEN)
		printf("DSOS_ESEMAPHOREOPEN\n\n");
	else
		printf("%d", openA);

	printf("\n\nCall: disastrOS_semOpen(0, DSOS_CREATE, 2)\n");
	int openC = disastrOS_semOpen(id[0], DSOS_CREATE, 2);
	printf("Returns: %d\n\n", openC);
	//disastrOS_printStatus();

	printf("\n\nCall: disastrOS_semOpen(0, DSOS_CREATE|DSOS_EXCL, 0)\nExpected: DSOS_ESEMAPHORENOEXCL\nReturns: ");
	int openD = disastrOS_semOpen(id[0], DSOS_CREATE | DSOS_EXCL, 0);
	if (openD == DSOS_ESEMAPHORENOEXCL)
		printf("DSOS_ESEMAPHORENOEXCL\n\n");
	else
		printf("%d", openD);

	printf("\n\nCall: disastrOS_semOpen(1, DSOS_CREATE|DSOS_EXCL, 1)\n");
	int openE = disastrOS_semOpen(id[1], DSOS_CREATE | DSOS_EXCL, 1);
	printf("Returns: %d\n\n", openE);
	//disastrOS_printStatus();

	printf("\n\nCall: disastrOS_semOpen(1, 0)\n");
	int openF = disastrOS_semOpen(id[1], 0);
	printf("Returns: %d\n\n", openF);
	//disastrOS_printStatus();

	printf("\n\nCall: disastrOS_semOpen(1, DSOS_CREATE, 0)\n");
	int openG = disastrOS_semOpen(id[1], DSOS_CREATE, 0);
	printf("Returns: %d\n\n", openG);

	printf("\n\nCall: disastrOS_semOpen(0, DSOS_CREATE, 2)\n");
	int openI = disastrOS_semOpen(id[0], DSOS_CREATE, 0);
	printf("Returns: %d\n\n", openI);

	printf("\n\nCall: disastrOS_semOpen(0, 0)\n");
	int openJ = disastrOS_semOpen(id[0], 0);
	printf("Returns: %d\n\n", openJ);

	disastrOS_printStatus();

	int waitE = disastrOS_semWait(openE);
	printf("\n\nCall: disastrOS_semWait(%d) = %d\n", openE, waitE);
	int postE = disastrOS_semPost(openE);
	printf("\n\nCall: disastrOS_semPost(%d) = %d\n", openE, postE);

	int waitI = disastrOS_semWait(openI);
	printf("\n\nCall: disastrOS_semWait(%d) = %d\n", openI, waitI);
	int waitJ = disastrOS_semWait(openJ);
	printf("\n\nCall: disastrOS_semWait(%d) = %d\n", openJ, waitJ);
	int postJ = disastrOS_semPost(openJ);
	printf("\n\nCall: disastrOS_semPost(%d) = %d\n", openJ, postJ);
	int postI = disastrOS_semPost(openI);
	printf("\n\nCall: disastrOS_semPost(%d) = %d\n", openI, postI);

	printf("\n\nCall: disastrOS_semClose(0)\n");
	int closeA = disastrOS_semClose(id[0]);
	printf("Returns: %d\n\n", closeA);

	printf("\n\nCall: disastrOS_semClose(1)\n");
	int closeB = disastrOS_semClose(id[1]);
	printf("Returns: %d\n\n", closeB);
	
	disastrOS_printStatus();

	printf("\n\nNow it's time to setup a bounded buffer problem.\n\nSpawn sleeperFunction.\n\n");

	disastrOS_spawn(sleeperFunction, 0);

	int ret;
	read_index = 0;
	write_index = 0;
	int cons_ids[CONS_NUM];
	int prod_ids[PROD_NUM];

	ret = disastrOS_semOpen(FILL_COUNT, DSOS_CREATE|DSOS_EXCL, 0);
	//printf("disastrOS_semOpen(FILL_COUNT, DSOS_CREATE|DSOS_EXCL, 0): %d\n", ret);
	ret = disastrOS_semOpen(EMPT_COUNT, DSOS_CREATE|DSOS_EXCL, BUFFER_SIZE);
	//printf("disastrOS_semOpen(EMPT_COUNT, DSOS_CREATE|DSOS_EXCL, BUFFER_SIZE): %d\n", ret);
	ret = disastrOS_semOpen(CONS_MUTEX, DSOS_CREATE|DSOS_EXCL, 1);
	//printf("disastrOS_semOpen(CONS_MUTEX, DSOS_CREATE|DSOS_EXCL, 1): %d\n", ret);
	ret = disastrOS_semOpen(PROD_MUTEX, DSOS_CREATE|DSOS_EXCL, 1);
	//printf("disastrOS_semOpen(PROD_MUTEX, DSOS_CREATE|DSOS_EXCL, 1): %d\n", ret);

	printf("\nSpawning %d consumer... ", CONS_NUM);
	for (int i = 0; i < CONS_NUM; i++) {
		cons_ids[i] = i+1;
		disastrOS_spawn(consFunction, &cons_ids[i]);
	}
	printf("done.\n");

	printf("\nSpawning %d producer... ", PROD_NUM);
	for (int i = 0; i < PROD_NUM; i++) {
		prod_ids[i] = i+1;
		disastrOS_spawn(prodFunction, &prod_ids[i]);
	}

	//disastrOS_printStatus();

	printf("done.\n\nWaiting for the children to complete their task...\n");
	for (int i = 0; i < PROD_NUM+CONS_NUM; i++) {
		disastrOS_wait(0, &ret);
		assert(!ret);
		//disastrOS_printStatus();
	}
	printf("\nClosing semaphores in parent function...\n\n");

	ret = disastrOS_semClose(FILL_COUNT);
	//printf("disastrOS_semClose(FILL_COUNT): %d\n", ret);
	ret = disastrOS_semClose(EMPT_COUNT);
	//printf("disastrOS_semClose(EMPT_COUNT): %d\n", ret);
	ret = disastrOS_semClose(CONS_MUTEX);
	//printf("disastrOS_semClose(CONS_MUTEX): %d\n", ret);
	ret = disastrOS_semClose(PROD_MUTEX);
	//printf("disastrOS_semClose(PROD_MUTEX): %d\n", ret);

	disastrOS_printStatus();

	printf("\n\nShutting down...\n\n");

	disastrOS_shutdown();
}

int main(int argc, char** argv) {
	char* logfilename = 0;
	if (argc > 1) {
		logfilename = argv[1];
	}
	// we create the init process processes
	// spawn an init process
	printf("\nLoading initFunction in disastrOS...\n\n\n");
	disastrOS_start(initFunction, 0, logfilename);
	return 0;
}
