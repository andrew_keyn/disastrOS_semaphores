#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semClose() {
	int semaphore_id = running->syscall_args[0];
	int ret;

	Semaphore* sem = SemaphoreList_byId(&semaphores_list, semaphore_id);

	if (!sem) {
		running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
		return;
	}

	SemDescriptor* sem_des = SemDescriptor_bySemId_pID(&sem->descriptors_ptrs, semaphore_id, running->pid);

	if (!sem_des) {
		running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
		return;
	}
	List_detach(&running->sem_descriptors, (ListItem*)sem_des);
	ret = SemDescriptor_free(sem_des);
	
	if (ret) {
		running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
		return;
	}

	SemDescriptorPtr* sem_des_ptr = sem_des->ptr;
	
	if (!sem_des_ptr) {
		running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
		return;
	}
	List_detach(&sem->descriptors_ptrs, (ListItem*)sem_des_ptr);
	ret = SemDescriptorPtr_free(sem_des_ptr);
	
	if (ret) {
		running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
		return;
	}

	//Se non vi sono altri processi che usano il semaforo questi viene rimosso

	if (sem->descriptors_ptrs.size == 0) {
		List_detach(&semaphores_list, (ListItem*) sem);
		ret = Semaphore_free(sem);

		if (ret) {
			running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
			return;
		}
	}

	running->syscall_retvalue = 0;
	return;
}