#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semOpen() {
	int semaphore_id = running->syscall_args[0];
	int mode = running->syscall_args[1];
	int count = running->syscall_args[2];

	if ((mode == 0) | (mode == DSOS_CREATE) | (mode == (DSOS_CREATE | DSOS_EXCL))) {
		
		Semaphore* sem = SemaphoreList_byId(&semaphores_list, semaphore_id);

		if (mode == 0 && sem == 0) {
			running->syscall_retvalue = DSOS_ESEMAPHOREOPEN;
			return;
		}

		//CREATE|EXCL: se esiste errore altrimenti allocazione e inserimento nella lista globale
		if (mode == (DSOS_CREATE | DSOS_EXCL)) {
			if (sem) {
				running->syscall_retvalue = DSOS_ESEMAPHORENOEXCL;
				return;
			}
			sem = Semaphore_alloc(semaphore_id, count);
			List_insert(&semaphores_list, semaphores_list.last, (ListItem*) sem);
		}

		//CREATE: se non esiste allocazione e inserimento nella lista globale
		if (mode == DSOS_CREATE) {
			if (!sem) {
				sem = Semaphore_alloc(semaphore_id, count);
				List_insert(&semaphores_list, semaphores_list.last, (ListItem*) sem);
			}
		}

		SemDescriptor* sem_des = SemDescriptor_bySemId_pID(&sem->descriptors_ptrs, semaphore_id, running->pid);

		if (!sem_des) {
			//Crea un decrittore per il semaforo nel processo corrente e aggiungilo alla lista nel suo pcb
			sem_des = SemDescriptor_alloc(running->last_sem_fd, sem, running);
			if (!sem_des) {
				running->syscall_retvalue = DSOS_ESEMAPHORENOFD;
				return;
			}
			running->last_sem_fd++;
			List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*) sem_des);

			//Crea un puntatore a descrittore e mettilo nella lista dei puntatori a descrittore del semaforo
			SemDescriptorPtr* sem_desptr = SemDescriptorPtr_alloc(sem_des);
			sem_des->ptr = sem_desptr;							//va inserito anche nel descrittore
			List_insert(&sem->descriptors_ptrs, sem->descriptors_ptrs.last, (ListItem*) sem_desptr);
		}

		// ritorna al processo il fd del nuovo descrittore
		running->syscall_retvalue = sem_des->fd;
		return;
	}

	running->syscall_retvalue = DSOS_EINVALIDARGUMENT;
	return;
}
