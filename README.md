# disastrOS_semaphores
Implementation of semaphores for the didactic operative system [disastrOS](https://gitlab.com/grisetti/sistemi_operativi_2016_17).

## Project overview
The aim of the project is to provide an implementation of semaphores to control access to a common resource by multiple processes in disastrOS, the didactic operative system created by [Prof. Giorgio Grisetti](https://sites.google.com/dis.uniroma1.it/grisetti) in the scope of the course [Sistemi Operativi](https://sites.google.com/dis.uniroma1.it/sistemi-operativi/), Fall/Winter 2017, La Sapienza [in italian].

The initial commit is part of the the teaching material of the course.

- *linked_list.h/linked_list.c* define the ListItem struct and related functions as a base for most of the following data structures
- *pool_allocator.h/pool_allocator.c* define the PoolAllocator struct and related functions for memory management
- *disastrOS_pcb.h/disastrOS_pcb.c* define the process control block structure and related functions
- *disastrOS_constants.h* defines system costants used within the os
- *disastrOS.h/disastrOS.c* provide system initialization and syscall mechanism
- *disastrOS_globals.h* defines global variables used by disastrOS
- *disastrOS_syscalls.h* defines disastrOS syscalls implemented in:
  - *disastrOS_preempt.c*
  - *disastrOS_fork.c*
  - *disastrOS_exit.c*
  - *disastrOS_wait.c*
  - *disastrOS_spawn.c*
  - *disastrOS_shutdown.c*
  - *disastrOS_schedule.c*
  - *disastrOS_sleep.c*
  - *disastrOS_openResource.c*
  - *disastrOS_closeResource.c*
  - *disastrOS_destroyResource.c*
  - *disastrOS_semOpen.c**
  - *disastrOS_semClose.c**
  - *disastrOS_semPost.c**
  - *disastrOS_semWait.c**
- *disastrOS_timer.h/disastrOS_timer.c* provide timer awakening mechanism
- *disastrOS_resource.h/disastrOS_resource.c* provide resource management mechanism
- *disastrOS_descriptor.h/disastrOS_descriptor.c* define the Descriptor struct and related functions for a process to keep track of the open resources
- *disastrOS_semaphore.h/disastrOS_semaphore.c* define the Semaphore struct and related functions for shared resources access control
- *disastrOS_test.c* simulates a sequence of OS operations and writes to the console the system state while running
- *disastrOS_semtest.c** executes a sequence of operations to test the results of the project

### How

Since disastrOS semaphores can be shared amongst different processes through a same id, the semantic implemented is going to be inspired by [Posix Semaphores](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/semaphore.h.html).
#### int disastrOS_semOpen(int semaphore_id, int mode, ...)
If DSOS_CREATE is specified in *mode*, then *count* argument must be supplied. If DSOS_CREATE is specified and a semaphore with the given name already exists, then *count* is ignored. If both DSOS_CREATE and DSOS_EXCL are specified in *mode*, then an error is returned if a semaphore with the given name already exists. Multiple calls within a same process return the same *fd*.
#### int disastrOS_semClose(int semaphore_id)
Removes the link between the calling process and the semaphore identified by *semaphore_id* deallocating the SemDescriptor and SemDescriptor_Ptr associated. If such link is not found an error is returned. If the calling process is the last one using the semaphore, the semaphore is deallocated from the system.
#### int disastrOS_semWait(int fd)
Locks the semaphore referenced by *fd*. If the semaphore value is currently zero, then the calling process shall not return from the call until it locks the semaphore. Upon successful return, the state of the semaphore shall be locked and shall remain locked until the semPost() function is executed and returns successfully.
#### int disastrOS_semPost(int fd)
Unocks the semaphore referenced by *fd*. Upon successful return, one of the processes waiting after a semWait() operation is set Ready for execution and the calling process hands over the cpu to the process on top of ready queue (enhancing disastrOS interleaving).

### How to run

```sh
git clone https://gitlab.com/andrew_keyn/disastrOS_semaphores.git
cd disastrOS_semaphores/src/
make
./disastrOS_semtest
```
